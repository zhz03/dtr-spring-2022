/*How t Use:
 * 1. Open Serial Monitor
 * 2. Input the pin (4,5,6,7)
 * 3. Input the speed (1000-2000)
 * 
 */


#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>

// called this way, it uses the default address 0x40
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
// you can also call it with a different address you want
//Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x41);
// you can also call it with a different address and I2C interface
//Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x40, Wire);

// Depending on your servo make, the pulse width min and max may vary, you 
// want these to be as small/large as possible without hitting the hard stop
// for max range. You'll have to tweak them as necessary to match the servos you
// have!
#define SERVOMIN  0 // This is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX  4096 // This is the 'maximum' pulse length count (out of 4096)
#define USMIN  1000 // This is the rounded 'minimum' microsecond length based on the minimum pulse of 150
#define USMAX  2000 // This is the rounded 'maximum' microsecond length based on the maximum pulse of 600
#define SERVO_FREQ 50 // Analog servos run at ~50 Hz updates

// our servo # counter
uint8_t servonum = 7;
uint16_t s = 1500;
uint8_t pin_number = 7;

void setup() {
  Serial.begin(115200);
  Serial.println("8 channel Servo test!");

  pwm.begin();
  pwm.setOscillatorFrequency(27000000);
  pwm.setPWMFreq(SERVO_FREQ);  // Analog servos run at ~50 Hz updates
  pwm.writeMicroseconds(7, 1000);
  pwm.writeMicroseconds(6, 1000);
  pwm.writeMicroseconds(5, 1000);
  pwm.writeMicroseconds(4, 1000);
  delay(1000);
  pwm.writeMicroseconds(7, s);
  pwm.writeMicroseconds(6, s);
  pwm.writeMicroseconds(5, s);
  pwm.writeMicroseconds(4, s);
  delay(1000);
  pwm.writeMicroseconds(7, 1300);
  pwm.writeMicroseconds(6, 1300);
  pwm.writeMicroseconds(5, 1300);
  pwm.writeMicroseconds(4, 1300);
  delay(1000);
}

// You can use this function if you'd like to set the pulse length in seconds
// e.g. setServoPulse(0, 0.001) is a ~1 millisecond pulse width. It's not precise!
void setServoPulse(uint8_t n, double pulse) {
  double pulselength;
  
  pulselength = 1000000;   // 1,000,000 us per second
  pulselength /= SERVO_FREQ;   // Analog servos run at ~60 Hz updates
  Serial.print(pulselength); Serial.println(" us per period"); 
  pulselength /= 4096;  // 12 bits of resolution
  Serial.print(pulselength); Serial.println(" us per bit"); 
  pulse *= 1000000;  // convert input seconds to us
  pulse /= pulselength;
  Serial.println(pulse);
  pwm.setPWM(n, 0, pulse);
}

void loop() {
  if (Serial.available() > 0){
    String k = "";
    k = Serial.readString();
    pin_number = k.toInt();
    Serial.print("new pin is: ");
    Serial.println(pin_number);
    while (!Serial.available()){}
    uint16_t spd = 0; 
    k = Serial.readString();
    s = k.toInt();
    Serial.print("new speed is: ");
    Serial.println(s);
    pwm.writeMicroseconds(pin_number, s);
  }
  delay(50);
//
//  // Drive each servo one at a time using writeMicroseconds(), it's not precise due to calculation rounding!
//  // The writeMicroseconds() function is used to mimic the Arduino Servo library writeMicroseconds() behavior. 
//  for (uint16_t microsec = USMIN; microsec < USMAX; microsec++) {
//    pwm.writeMicroseconds(servonum, microsec);
//  }
//
//  delay(50);
//  for (uint16_t microsec = USMAX; microsec > USMIN; microsec--) {
//    pwm.writeMicroseconds(servonum, microsec);
//  }
//  delay(50);

}
