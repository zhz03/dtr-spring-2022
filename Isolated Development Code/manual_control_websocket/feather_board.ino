#include <Arduino.h>

#include <WiFi.h>
#include <WebServer.h>
#include <WiFiClient.h>
#include <WebSocketsServer.h>

#include <Wire.h>
// #include "Adafruit_VL53L0X.h"


#include "debug.h"
#include "server.h"



const int USMIN = 1000; // This is the rounded 'minimum' microsecond length based on the minimum pulse of 150
const int USMAX = 2000; // This is the rounded 'maximum' microsecond length based on the maximum pulse of 600
const int BRUSHLESS_FREQ = 50; // brushless motors run at ~50 Hz updates
const int OSCI_FREQ = 27000000;

#include <Adafruit_PWMServoDriver.h>
static const uint8_t l_motor = 7;
static const uint8_t r_motor = 6;
static const uint8_t vertical_motor_1 = 5;
static const uint8_t vertical_motor_2 = 4;
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();

// WiFi AP parameters
char ap_ssid[13];
char* ap_password = "";

// WiFi STA parameters
/*
char* sta_ssid = "NETGEAR33";
char* sta_password = "freestreet163";
*/

char* sta_ssid = "lemur";
char* sta_password = "lemur9473";

void setupPins() {
    // setup Serial and actuators
    Serial.begin(115200);
    DEBUG("Started serial.");
    pinMode(LED_BUILTIN, OUTPUT);
    DEBUG("Setup pins");
}

void setmotors(const uint8_t payload[10]) {
    digitalWrite(LED_BUILTIN, payload[9]);
//    motorVert1->run(payload[1]);
//    motorVert1->setSpeed(payload[2]);
//    motorVert2->run(payload[3]);
//    motorVert2->setSpeed(payload[4]);
//    motorLeft->run(payload[5]);
//    motorLeft->setSpeed(payload[6]);
//    motorRight->run(payload[7]);
//    motorRight->setSpeed(payload[8]);
  
  int l_speed = 0;
  int r_speed = 0;
  int v_speed = 0;

  if (payload[1] == 0){
    v_speed = -1*payload[2];  
  } else {
    v_speed = payload[2]; 
  }

  if (payload[5] == 0){
    l_speed = -1*payload[6];  
  } else {
    l_speed = payload[6]; 
  }

  if (payload[7] == 0){
    r_speed = -1*payload[8];  
  } else {
    r_speed = payload[8]; 
  }

  l_speed = map(l_speed, -255, 255, 1000, 2000);
  r_speed = map(r_speed, -255, 255, 1000, 2000);
  v_speed = map(v_speed, -255, 255, 1000, 2000);

  //run the motors
  pwm.writeMicroseconds(l_motor, l_speed);
  pwm.writeMicroseconds(r_motor, r_speed);
  pwm.writeMicroseconds(vertical_motor_1, v_speed);
  pwm.writeMicroseconds(vertical_motor_2, v_speed);
}

void stop() {
    const static uint8_t off[10] = {0,0,0,0,0,0,0,0,0,0};
    setmotors(off);
}

void webSocketEvent(uint8_t id, WStype_t type, uint8_t * payload, size_t length) {
    char ipstr[16];

    switch(type) {
        case WStype_DISCONNECTED:
            DEBUG("Web socket disconnected, id = ", id);
            break;

        case WStype_CONNECTED: 
            // IPAddress ip = webSocket.remoteIP(id);
            // sprintf(ipstr, "%d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
            DEBUG("Web socket connected, id = ", id);
            // DEBUG("  from: ", ipstr);
            DEBUG("  url: ", (char *)payload);

            // send message to client
            wsSend(id, "Connected to ");
            wsSend(id, ap_ssid);
            break;

        case WStype_BIN:
            DEBUG("On connection #", id)
            DEBUG("  got binary of length ", length);
            for (int i = 0; i < length; i++)
              DEBUG("    char : ", payload[i]);
            if (payload[0] == '~' && length == 10) 
              setmotors(payload);
            break;

        case WStype_TEXT:
            DEBUG("On connection #", id)
            DEBUG("  got text: ", (char *)payload);
            break;
    }
//    VL53L0X_RangingMeasurementData_t measure;
//    lox.rangingTest(&measure, false);
//    DEBUG("Lidar Value : ", measure.RangeMilliMeter);
//    wsSendInt(id, measure.RangeMilliMeter);    
}

void setup() {
    setupPins();
    
    for(uint8_t t = 4; t > 0; t--) {
        Serial.printf("[SETUP] BOOT WAIT %d...\n", t);
        Serial.flush();
    }

//    if (!lox.begin()) {
//      Serial.println(F("Failed to boot VL53L0X"));
//      while(1);
//    }

    setupSTA(sta_ssid, sta_password);
    //setupAP(ap_ssid, ap_password);
    setupWS(webSocketEvent);
    pwm.begin();
    pwm.setOscillatorFrequency(OSCI_FREQ);
    pwm.setPWMFreq(BRUSHLESS_FREQ);  // Analog servos run at ~50 Hz updates
    pwm.writeMicroseconds(l_motor, 1500);
    pwm.writeMicroseconds(r_motor, 1500);
    pwm.writeMicroseconds(vertical_motor_1, 1500);
    pwm.writeMicroseconds(vertical_motor_2, 1500);
}

void loop() {
    wsLoop();
}
