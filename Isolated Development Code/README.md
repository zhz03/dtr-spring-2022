# ESP-EYE Setup!

## Steps to get ESP-EYE setup in your Arduino IDE:
1. Download the Arduino IDE
2. Install [ESP32 add-on](https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/) in your Arduino IDE. Follow all the instructions including testing the ESP-EYE with example "WifiScan".

## Troubleshoot:
1. If you get prompted by your Arduino IDE that ```Text section exceeds available space in board```
    - In your Arduino IDE, go to **Tools > Partition Scheme > Huge APP (3MB No OTA/1MB SPIFFS)**. This gives Arduino IDE more storage space to run your program.
