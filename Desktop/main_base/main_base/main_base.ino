#include <PS4Controller.h>
#include <esp_now.h>
#include <WiFi.h>

#define DDRIVE_MIN -128 //The minimum value x or y can be.
#define DDRIVE_MAX 127  //The maximum value x or y can be.
#define MOTOR_MIN_PWM -255 //The minimum value the motor output can be.
#define MOTOR_MAX_PWM 255 //The maximum value the motor output can be.
#define MID_PT 0

int LeftMotorOutput; //will hold the calculated output for the left motor
int RightMotorOutput; //will hold the calculated output for the right motor
int VerticalMotorOutput;
bool manual_mode = true;
bool debug_mode_controller = true;

// REPLACE WITH YOUR RECEIVER MAC Address
uint8_t broadcastAddress[] = {0x40, 0xf5, 0x20, 0x44, 0xbe, 0x84}; //40:F5:20:44:BE:84

typedef struct struct_message {
  int left_output;
  int right_output;
  int vertical_output;
  int catcher_output;
  bool manual;
} struct_message;

struct_message message;

void data_sent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nStatus of Last Message Sent:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

void setup() {
  Serial.begin(115200);
  PS4.begin("d8:c0:a6:38:c2:e7");
  WiFi.mode(WIFI_STA);
  
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  esp_now_register_send_cb(data_sent);
  esp_now_peer_info_t peerInfo = {};
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;     
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }
  Serial.println("Ready.");
}

void loop() {
  if (PS4.isConnected()) {
    Serial.println("PS4 controller is connected!");
  }

  while(PS4.isConnected()){
    if (PS4.LStickX() || PS4.LStickY()) {
      CalculateMotorOutput(float(PS4.LStickX()), float(PS4.LStickY()));
      if (abs(LeftMotorOutput) < 25){
        LeftMotorOutput = 0;
      }
      if (abs(RightMotorOutput) < 25){
        RightMotorOutput = 0;
      }
    }

    if (PS4.RStickY()) {
      VerticalMotorOutput = map(PS4.RStickY(), DDRIVE_MIN, DDRIVE_MAX, MOTOR_MIN_PWM, MOTOR_MAX_PWM);
      if (abs(VerticalMotorOutput) < 25){
        VerticalMotorOutput = 0;
      }
    }

    if (PS4.L1()){ //MANUAL MODE
      manual_mode = true;
    }

    if (PS4.R1()){ //AUTO MODE
      manual_mode = false;
    }

    if (PS4.R2()){
      message.catcher_output = 1700;
    } else if (PS4.L2()){
      message.catcher_output = 1100;
    } else {
      message.catcher_output = 1350; 
    }

    if (debug_mode_controller){
      Serial.print("LEFT MOTOR: ");
      Serial.println(LeftMotorOutput);
      Serial.print("RIGHT MOTOR: ");
      Serial.println(RightMotorOutput);
      Serial.printf("Left Stick x at %d\n", PS4.LStickX());
      Serial.printf("Left Stick y at %d\n", PS4.LStickY()); 
      Serial.printf("Right Stick y at %d\n", PS4.RStickY());
      Serial.printf("R2 at %d\n", PS4.R2());
      Serial.printf("L2 at %d\n", PS4.L2());
      Serial.print("MANUAL MODE: ");
      if (manual_mode){
        Serial.println("ON"); 
      } else {
        Serial.println("OFF"); 
      }
    }
    
    if (PS4.L1() || manual_mode || PS4.R2() || PS4.L2()){
      message.left_output = int(LeftMotorOutput);
      message.right_output = int(RightMotorOutput);
      message.vertical_output = int(VerticalMotorOutput);
      esp_err_t outcome = esp_now_send(broadcastAddress, (uint8_t *) &message, sizeof(message));
      message.manual = manual_mode;
    } else if (PS4.R1()) {
      message.left_output = 0;
      message.right_output = 0;
      message.vertical_output = 0;
      message.manual = manual_mode;
      message.catcher_output = 1350;
      esp_err_t outcome = esp_now_send(broadcastAddress, (uint8_t *) &message, sizeof(message));
    }
    
   
//    if (outcome == ESP_OK) {
//      Serial.println("Mesage sent successfully!");
//    }
//    else {  
//      Serial.println("Error sending the message");
//    }
    delay(50);
  }
}


void CalculateMotorOutput(float x, float y) {
  float rawLeft;
  float rawRight;
  float RawLeft;
  float RawRight;

  // first Compute the angle in deg
  // First hypotenuse
  float z = sqrt(x * x + y * y);

  // angle in radians
  float rad = acos(abs(x) / z);

  // Cataer for NaN values
  if (isnan(rad) == true) {
    rad = 0;
  }

  // and in degrees
  float angle = rad * 180 / PI;

  // Now angle indicates the measure of turn
  // Along a straight line, with an angle o, the turn co-efficient is same
  // this applies for angles between 0-90, with angle 0 the co-eff is -1
  // with angle 45, the co-efficient is 0 and with angle 90, it is 1

  float tcoeff = -1 + (angle / 90) * 2;
  float turn = tcoeff * abs(abs(y) - abs(x));
  turn = round(turn * 100) / 100;

  // And max of y or x is the movement
  float mov = max(abs(y), abs(x));

  // First and third quadrant
  if ((x >= 0 && y >= 0) || (x < 0 && y < 0)){
    rawLeft = mov; rawRight = turn;
  } else {
    rawRight = mov; rawLeft = turn;
  }

  // Reverse polarity
  if (y < 0) {
    rawLeft = 0 - rawLeft;
    rawRight = 0 - rawRight;
  }

  // Update the values
  RawLeft = rawLeft;
  RawRight = rawRight;

  // Map the values onto the defined rang
  LeftMotorOutput = map(rawLeft, DDRIVE_MIN, DDRIVE_MAX, MOTOR_MIN_PWM, MOTOR_MAX_PWM);
  RightMotorOutput = map(rawRight, DDRIVE_MIN, DDRIVE_MAX, MOTOR_MIN_PWM, MOTOR_MAX_PWM);
}
