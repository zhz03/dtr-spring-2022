#include <esp_now.h>
#include <WiFi.h>

//typedef struct struct_message {
////    char character[100];
////    int integer;
////    float floating_value;
////    bool bool_value;
//} struct_message;

typedef struct struct_message {
  int left_output;
  int right_output;
  int vertical_output;
  bool manual;
} struct_message;

struct_message message;

void data_receive(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&message, incomingData, sizeof(message));
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("L: ");
  Serial.println(message.left_output);
  Serial.print("R: ");
  Serial.println(message.right_output);
  Serial.print("Vertical: ");
  Serial.println(message.vertical_output);
  Serial.print("Manual: ");
  Serial.println(message.manual);
  Serial.println();
}
 
void setup() {
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.setSleep(false);
  Serial.println(WiFi.macAddress());

  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }
  
  esp_now_register_recv_cb(data_receive);
}
 
void loop() {

}
