/*
 * Input:       int corresponding to camera number
 * Output:      none
 * Description: prints out the debug details of the cameras in the blimp
 *              CAMERA 1 = BOTTOM CAMERA
 *              CAMERA 2 = FRONT CAMERA
 */
void printCamera(int camera_num){
  if (camera_num ==1){
    Serial.print("BOTTOM CAMERA - detected x = ");
    Serial.println(btm_cam_x);
    Serial.print("BOTTOM CAMERA - detected y = ");
    Serial.println(btm_cam_y);
    Serial.print("BOTTOM CAMERA - detected area = ");
    Serial.println(btm_cam_area);
  }
}
