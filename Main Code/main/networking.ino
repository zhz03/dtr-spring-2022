void moveBlimp(int l_speed, int r_speed, int v_speed);

void data_receive(const uint8_t * mac, const uint8_t *incomingData, int len) {
  memcpy(&message, incomingData, sizeof(message));
  moveBlimp(message.left_output, message.right_output, message.vertical_output);
  manual_mode = message.manual;
  pwm.writeMicroseconds(catcher_servo, message.catcher_output);
  Serial.print("CATCHER: ");
  Serial.println(message.catcher_output);
  command_time = millis();
}
