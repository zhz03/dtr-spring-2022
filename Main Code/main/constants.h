/*
 * In this sub-part, the global constants that the blimp will use will be stored here
 * the constants are by convention defined with an all upper case name.
 */

#ifndef CONSTANTS
#define CONSTANTS

  const int BAUD_RATE = 115200;
  const int TIME_STEP = 50;
  const int MAX_SPEED = 255;
  const int BASE_THRUST_SPEED = 50;
  const int LIDAR_TIMEOUT = 100;

  const double CAMERA_MIDPOINT_X = 160;
  const double CAMERA_MIDPOINT_Y = 120;
  const int PID_ABS_MAX_OUTPUT = 255;
  const int TARGET_AREA_HOVERING = 5000;

  const int USMIN = 1000; // This is the rounded 'minimum' microsecond length based on the minimum pulse of 150
  const int USMAX = 2000; // This is the rounded 'maximum' microsecond length based on the maximum pulse of 600
  const int BRUSHLESS_FREQ = 200; // brushless motors run at ~50 Hz updates
  const int OSCI_FREQ = 27000000;

  const int STABLE_HOVERING_COUNTER_THRESHOLD = 20; //the timestep needed before transitioning to going down to the green ball
  const int STABLE_HOVERING_AREA_THRESHOLD = 30;

  const int MAX_DIFF_INPUT = 100;
  
#endif
