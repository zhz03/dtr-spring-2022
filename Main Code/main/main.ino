/*
 * Main Code for the UCLA LEMUR Autonomous Blimp Project
 * 
 * Description:
 * 
 * Author(s):
 *  1. Shahrul Kamil bin Hassan
 *  2. 
 * 
 */

//LIBRARIES
#include <Arduino.h>
#include <string>
#include <vector>
#include <Servo.h>
#include <PID_v1.h>
#include <Wire.h>
#include <esp_now.h>
#include <WiFi.h>
#include <Adafruit_PWMServoDriver.h>
#include "constants.h"
#include "Adafruit_VL53L0X.h"
#include "Camera.h"


//PINS
static const int servoPin = 26;
static const int cameraPin = 13;
static const uint8_t l_motor = 7;
static const uint8_t r_motor = 6;
static const uint8_t vertical_motor_1 = 5;
static const uint8_t vertical_motor_2 = 4;
static const uint8_t catcher_servo = 3;


//GLOBAL VARIABLES
String current_state = "";      //indicate the current state of the system
String previous_state = "";     //record the last known state of the system
bool claw_open = true;
int stable_hovering_counter = 0;
bool manual_mode = true;
int command_time = 0; 

//DEBUG VARIABLES
bool debug_mode_lidar = true;  //print out debug data of the lidar to the serial monitor
bool debug_mode_btm_cam = true; //
bool debug_mode_horizontal_PID = true;
bool debug_mode_vertical_PID = true;
bool debug_mode_motor_actuation = true;
bool debug_mode_ball_hovering = true;

//PID CONTROL VARIABLES
double Setpointx, Inputx, Outputx;
double Setpointy, Inputy, Outputy;
double Setpointz, Inputz, Outputz;

double Kpx=0.5, Kix=0, Kdx=0.15;
double Kpy=0.5, Kiy=0, Kdy=0.15;
double Kpz=0.03, Kiz=0, Kdz=0.007;

//CAMERA VISUAL DETECTION VARIABLES
int8_t threshold[6] = {30, 100, -93, -5, 13, 127}; //need to be set to proper LAB values
int btm_cam_x = 0;
int btm_cam_y = 0;
int btm_cam_area = 0;

//PREVIOUS MOTOR INPUTS
int prev_left = (USMIN + USMAX)/2;
int prev_right = (USMIN + USMAX)/2;
int prev_vertical = (USMIN + USMAX)/2;

//CAMERA INTERFACES
openmv::rpc_scratch_buffer<256> scratch_buffer; // All RPC objects share this buffer.
openmv::rpc_spi_master interface(cameraPin, 1000000, SPI_MODE2);

//OBJECTS AND SENSORS
Adafruit_VL53L0X lidar = Adafruit_VL53L0X();
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver();
//Servo servo_claw;
PID PID_x(&Inputx, &Outputx, &Setpointx, Kpx, Kix, Kdx, DIRECT);
PID PID_y(&Inputy, &Outputy, &Setpointy, Kpy, Kiy, Kdy, DIRECT);
PID PID_z(&Inputz, &Outputz, &Setpointz, Kpz, Kiz, Kdz, DIRECT);
Camera cam(&interface);

typedef struct struct_message {
  int left_output;
  int right_output;
  int vertical_output;
  int catcher_output;
  bool manual;
} struct_message;

void data_receive(const uint8_t * mac, const uint8_t *incomingData, int len);
struct_message message;

void setup() {
  Serial.begin(BAUD_RATE);

  //Setting up the camera interface
  interface.begin(); //communication between ESP and OpenMV

  //Setting up the servo on the catching mechanism
  //servo_claw.attach(servoPin);

  //Setting up and making sure that the LIDAR is working
//  if (!lidar.begin()) {
//    Serial.println(F("Failed to boot VL53L0X"));
//    delay(LIDAR_TIMEOUT);
//  }

  //Setting up the PID control
  Setpointx = CAMERA_MIDPOINT_X; 
  Setpointy = CAMERA_MIDPOINT_Y;  //the values that the PID will try to reach
  Setpointz = TARGET_AREA_HOVERING;
  PID_x.SetOutputLimits(-255, 255); 
  PID_y.SetOutputLimits(-255, 255); 
  PID_z.SetOutputLimits(-255, 255);
  PID_x.SetMode(AUTOMATIC);
  PID_y.SetMode(AUTOMATIC);
  PID_z.SetMode(AUTOMATIC);

  //Setting up the networking
  WiFi.mode(WIFI_STA);
  WiFi.setSleep(false);
  Serial.println(WiFi.macAddress());
  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }  
  esp_now_register_recv_cb(data_receive);

  //Setting up the brushless motors
  pwm.begin();
  pwm.setOscillatorFrequency(OSCI_FREQ);
  pwm.setPWMFreq(BRUSHLESS_FREQ);  // Analog servos run at ~50 Hz updates
//  delay(1000);
//  pwm.writeMicroseconds(l_motor, 2000);
//  pwm.writeMicroseconds(r_motor, 2000);
//  pwm.writeMicroseconds(vertical_motor_1, 2000);
//  pwm.writeMicroseconds(vertical_motor_2, 2000);
//  delay(2000);
  pwm.writeMicroseconds(l_motor, 1500);
  pwm.writeMicroseconds(r_motor, 1500);
  pwm.writeMicroseconds(vertical_motor_1, 1500);
  pwm.writeMicroseconds(vertical_motor_2, 1500);
  delay(2000);
//  pwm.writeMicroseconds(l_motor, 1600);
//  pwm.writeMicroseconds(r_motor, 1600);
//  pwm.writeMicroseconds(vertical_motor_1, 1600);
//  pwm.writeMicroseconds(vertical_motor_2, 1600);
//  delay(2000);
}

void loop() {
  current_state = statePlanning();
  Serial.print("STATE - ");
  Serial.println(current_state);
  actuation(current_state);
  delay(TIME_STEP);
}
