/*
 * This file is used to separate the utility functions that will be used by the 
 * autonomous blimp
 */

void moveBlimp(int l_speed, int r_speed, int v_speed);

int test(){
  Serial.println(BAUD_RATE);
}

String statePlanning(){
  previous_state = current_state;
  String state = "";
  int dist = 300; //getLidarReading();
  int x = -1;
  int y = -1;
  if (manual_mode){
    return "MANUAL";
  }
  
//  if (dist < 200 && claw_open == true){
//    claw_open = false;
//    state = "CLOSE_CLAW";
//    return state;
//  } else if (dist > 200 && claw_open == false) {
//    claw_open = true;
//    state = "OPEN_CLAW";
//    return state;

  dist = 300;
  //if there is no need to change the catching mechanism, we move to the main control
  if (dist < 200) {
    Serial.println("...moving to goal"); //RUN the goal-seeking algorithm on front camera
  //  if (there is a goal){
  //    return STATE - MOVE_TO_GOAL
  //  } else { //there is no goal in sight
  //    if (previous state was MOVE_TO_GOAL) {
  //      return state - ADJUST_TO_REFIND_GOAL
  //    } else if (previous state was ADJUST_TO_REFIND_GOAL) {
  //      keep track of how long it has been       
  //      return STATE - ADJUST_TO_REFIND_GOAL
  //    } else { //goal was not visible for a while (either lost track or currently seeking)
  //      return STATE - SEEK_GOAL (this can be expanded )
  //    }
  //  }
  } else { //if there is no green ball in the capturing mechanism
    btm_cam_x = 0; btm_cam_y = 0; btm_cam_area = 0;
    bool ball_detected = cam.exe_color_detection_biggestblob(threshold[0], threshold[1], threshold[2], threshold[3], threshold[4], threshold[5], btm_cam_x, btm_cam_y, btm_cam_area);
    if (ball_detected) { //need to add and the ball is still not stable
      if (debug_mode_btm_cam){
        printCamera(1);
      }
      if (stableHoveringAchieved()){
        return "HOVER_ABOVE_BALL"; //"MOVE_TO_BALL";
      } else {
        return "HOVER_ABOVE_BALL"; 
      }
    } else { //if the green ball is not visible in the camera frame
      if (debug_mode_btm_cam){
        Serial.println("BOTTOM CAMERA - no ball detected");
      }
      if (previous_state == "MOVE_TO_BALL") {
        return "BALL_SEEKING";
      } else { //either we lost track or it is seeking
        return "BALL_SEEKING";
      }
    }
  }
  
  return state;
}

void actuation(String state){
  int code = stateCode(state);
  switch(code){
    case 0:
    {
//      for(int posDegrees = 180; posDegrees >= 0; posDegrees = posDegrees - 2) {
//        servo_claw.write(posDegrees);
//        //Serial.println(posDegrees);
//        delay(10);
//      }
        break;
    }  
    case 1:
    {
//      for(int posDegrees = 0; posDegrees <= 180; posDegrees = posDegrees + 2) {
//        servo_claw.write(posDegrees);
//        //Serial.println(posDegrees);
//        delay(10);
//      }
      break;
    } 
    case 2: //MOVE TO BALL
    {
      int l_speed = 0;
      int r_speed = 0;
      runHorizontalPID(btm_cam_x, btm_cam_y);
      runVerticalPID(btm_cam_area);
      horizontalPlaneMotorOutput(Outputx, Outputy, l_speed, r_speed);
      moveBlimp( l_speed, r_speed, -10); //-1*int(Outputz));
      break;
    }
    case 3: //BALL SEEKING
    {
      moveBlimp(0, 0, 0);
      stable_hovering_counter = 0;
      break;
    } 
    case 4: //HOVER ABOVE BALL
    {
      int l_speed = 0;
      int r_speed = 0;
      runHorizontalPID(btm_cam_x, btm_cam_y);
      runVerticalPID(btm_cam_area);
      horizontalPlaneMotorOutput(Outputx, Outputy, l_speed, r_speed);
      moveBlimp( l_speed, r_speed, -1*int(Outputz)); //-1*int(Outputz));
      break;
    }
    case 99: //MANUAL
    {
      //if (millis() - command_time > 500){
      //  moveBlimp(0,0,0);
      //}
      break;
    }
  }
}

/*
 * Input:       none
 * Output:      int corresponding to state in int
 * Description: convert Strings describing the state into int values for the switch 
 *              case in the actuation
 */
int stateCode(String state){
  int code = -1;
  if (state == "CLOSE_CLAW"){
    code = 0;
  } else if (state == "OPEN_CLAW"){
    code = 1;
  } else if (state == "MOVE_TO_BALL"){
    code = 2;
  } else if (state == "BALL_SEEKING"){
    code = 3;
  } else if (state == "HOVER_ABOVE_BALL"){
    code = 4;
  } else if (state == "MANUAL"){
    code = 99;
  }
  return code;
}




/*
 * Input:       none
 * Output:      bool corresponding to if the ball was detected by the algorithm
 * Description: With the camera that was set up, this function will determine if the
 *              ball was detected or not
 */
bool ballVisible(int x, int y){
  return (x > 0 && y >0);
}

bool stableHoveringAchieved(){ //NOTE: need to reset counter after ball is captured
  if (stable_hovering_counter > STABLE_HOVERING_COUNTER_THRESHOLD){
    return true;
  }

  //else
  if (abs(btm_cam_x - CAMERA_MIDPOINT_X) < STABLE_HOVERING_AREA_THRESHOLD && abs(btm_cam_y - CAMERA_MIDPOINT_Y) < STABLE_HOVERING_AREA_THRESHOLD) {
    stable_hovering_counter = stable_hovering_counter + 1;
  } else {
    stable_hovering_counter = 0;
  }

  if (debug_mode_ball_hovering){
    Serial.print("HOVERING - Counter = ");
    Serial.println(stable_hovering_counter);
  }
  return false;
}
 
/*
 * Input:       none
 * Output:      int corresponding to the lidar measurement reading in mm
 * Description: this is a function specifically for the VL53L0X lidar hardware. It is 
 *              used to get the measurement reading of the lidar
 */
//int getLidarReading(){
//  int reading = -1;
//  VL53L0X_RangingMeasurementData_t measure;
//  lidar.rangingTest(&measure, false);
//  if (measure.RangeStatus != 4) {  // phase failures have incorrect data
//    if(debug_mode_lidar){
//      Serial.print("LIDAR -     Distance (mm): "); 
//      Serial.println(measure.RangeMilliMeter);
//    }
//    reading = measure.RangeMilliMeter;
//  } else {
//    if(debug_mode_lidar){
//      Serial.println("LIDAR -     out of range ");
//    }
//    reading = 10000; //need to fix later
//  }
//  return reading;
//}

/*
 * Input:       int x - for the x position of the target
 *              int y - for the y position of the target
 * Output:      none (However, it will ovewrite the Outputx and Outputy variables)
 *              double Outputx - a value between (-255, 255), where -255 indicate the most
 *                               extreme right motion and 255 indicating extreme left
 *              double Outputy - a value between (-255, 255), where -255 indicate the most
 *                               extreme backward motion and 255 indicating extreme forward
 * Description: function that gives the output for the Horizontal PID, with fixed max
 *              amplitude of |255|. Note that the way the camera is set up, positive x move to 
 *              the right and positive y move down, with the origin at the top left of the frame
 *              
 */
void runHorizontalPID(int x, int y){
  //Set the input from the camera to be the input for the PIDs
  Inputx = x/1.00;
  Inputy = y/1.00;

  //Compute the PID output values
  PID_x.Compute();
  PID_y.Compute();

  if (debug_mode_horizontal_PID){
    Serial.print("HORIZONTAL PID - x output: ");
    Serial.println(Outputx);
    Serial.print("HORIZONTAL PID - y output: ");
    Serial.println(Outputy);
  }
}

/*
 * Input:       int z - a numerical measurement of how far the targeted object is
 * Output:      none (However, it will ovewrite the Outputz variable)
 *              double Outputz - a value between (-255, 255), where -255 indicate the most
 *                               extreme downward motion and 255 indicating extreme upward
 * Description: function that gives the output for the Vertical PID, with fixed max
 *              amplitude of |255|. 
 *              
 */
void runVerticalPID(int z){
  //Set the input from the camera to be the input for the PIDs
  Inputz = z/1.00;

  //Compute the PID output value
  PID_z.Compute();

  if (debug_mode_vertical_PID){
    Serial.print("VERTICAL PID - z output: ");
    Serial.println(Outputz);
  }
}

/*
 * Input:       double Outputx - PID output for the x direction
 *              double Outputy - PID output for the x direction
 *              int& l_speed - reference to an int for the motor 
 * Output:      none
 * Description: this function provides the motor output values needed to move the blimp in 
 *              the xy-plane. the clock directions described in this function is based on
 *              observing the blimp from a bird-eye view. The l_speed and the r_speed values 
 *              wil be set in a way such that a positive value will give a forward thrust
 *              to that specific motor
 */
void horizontalPlaneMotorOutput(double Outputx, double Outputy, int& l_speed, int& r_speed){
  l_speed = int(Outputx)*-1*(int(Outputy)/abs(int(Outputy))) + int(Outputy);
  r_speed = int(Outputx)*(int(Outputy)/abs(int(Outputy))) + int(Outputy);

  //Making sure that the values do not exceed a certain threshold (in this case |255|)
  l_speed = max(l_speed, -1*PID_ABS_MAX_OUTPUT);
  l_speed = min(l_speed,  PID_ABS_MAX_OUTPUT);
  r_speed = max(r_speed, -1*PID_ABS_MAX_OUTPUT);
  r_speed = min(r_speed, PID_ABS_MAX_OUTPUT);

  if (debug_mode_horizontal_PID){
    Serial.print("HORIZONTAL PID - left motor speed: ");
    Serial.println(l_speed);
    Serial.print("HORIZONTAL PID - right motor speed: ");
    Serial.println(r_speed);
  }
}

//THIS FUNCTION IS NOT COMPLETE
void moveBlimp(int l_speed, int r_speed, int v_speed){

  l_speed = map(l_speed, -255, 255, 1100, 1900);
  r_speed = map(r_speed, -255, 255, 1100, 1900);
  v_speed = map(v_speed, -255, 255, 1100, 1900);

  if (debug_mode_motor_actuation){
    Serial.print("MOTOR ACTUATION - left motor speed = ");
    Serial.println(l_speed);
    Serial.print("MOTOR ACTUATION - right motor speed = ");
    Serial.println(r_speed);
    Serial.print("MOTOR ACTUATION - vertical motors speed = ");
    Serial.println(v_speed);
  }

  //check in order to not have spike in input
  if (l_speed != 1500) { l_speed = max(min(prev_left + MAX_DIFF_INPUT, l_speed), prev_left - MAX_DIFF_INPUT); }
  if (r_speed != 1500) { r_speed = max(min(prev_right + MAX_DIFF_INPUT, r_speed), prev_right - MAX_DIFF_INPUT); }
  if (v_speed != 1500) { v_speed = max(min(prev_vertical + MAX_DIFF_INPUT, v_speed), prev_vertical - MAX_DIFF_INPUT); }
  
  //run the motors
  pwm.writeMicroseconds(l_motor, l_speed);
  pwm.writeMicroseconds(r_motor, r_speed);
  pwm.writeMicroseconds(vertical_motor_1, v_speed);
  pwm.writeMicroseconds(vertical_motor_2, v_speed);

  //overwrite the variables for previous motor output
  prev_left = l_speed;
  prev_right = r_speed;
  prev_vertical = v_speed;
  
}
