This folder is used to store codes that will be flashed onto the main microcontroller of the integrated system. Any codes and functional that were tested in isolation needs to be tested and integrated first before merging into the main branch. In order for the system to work, there's two separate esp32 feathers that needs to be set up. One is in this folder and the other one will be connected to the laptop as a base station. You can find the code for the base station here: [main_base](https://git.uclalemur.com/shahrulkamil98/dtr-spring-2022/-/tree/main/Desktop/main_base)

<details><summary> 1. Setting up ESP32 Feather with Arduino </summary>
<br />
Before flashing the code into the feather, we need to make sure that we have set up our Arduino IDE so that it can work with the feather. In case that has not been done yet, follow the instructions in the link below:
<br />
<br />
[ESP32 Feather with Arduino IDE Instructions](https://learn.adafruit.com/adafruit-huzzah32-esp32-feather/using-with-arduino-ide)

</details>

<details><summary>2. Libraries to Install</summary>
<br />
There are a few libraries that we are utilizing for the project. Please install them to ensure that you can get the code to compile when you want to flash it into the feather. Here is the list of libraries:
<br />
<br />
- https://github.com/adafruit/Adafruit_VL53L0X/blob/master/src/Adafruit_VL53L0X.h
<br />
- https://github.com/adafruit/Adafruit-PWM-Servo-Driver-Library/blob/master/Adafruit_PWMServoDriver.h
<br />
- https://github.com/espressif/esp-idf/blob/master/components/esp_wifi/include/esp_now.h
<br />
- https://github.com/br3ttb/Arduino-PID-Library/blob/master/PID_v1.h
<br />
<br />
The rest of the libraries in the code are built-in libraries that comes with either the Arduino IDE or the ESP32 Board package. Note that there are other libraries for the main_base.ino file that you will need to install.

</details>

<details><summary>3. Key Variables and Constants</summary>

</details>

<details><summary>4. System Block Diagram</summary>

</details>

<details><summary>5. How the System Works</summary>

</details>

