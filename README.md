**Project Description**

This in an active project being worked in UCLA LEMUR Lab. Our goal for this project is to create an autonomous blimp that would be able to do the following things:

    1. Identify a green balloon
    2. Move towards the green balloon
    3. Capture (hold on) to the green balloon
    4. Identify a goal
    5. Move towards the goal
    6. Move into the goal
    
The blimp needs to be lightweight enough so that while it is not active, the negative buoyancy is at most 50g. For more information regarding this project, and the competition we are going to go for the project, visit the link below:

Link: https://blimps.uclalemur.com/ 


**Component List**

For more information on what materials is needed to build our blimp, go to the Logistics directory in this repository to locate our full BOM file.

**Flashing Code**

In order to automate the blimp, the right code needs to be set up on the ESP32 Feather Board, the ESP-EYEs, and the laptop. Please go to the Main Code directory and follow the instructions to set up the software for this blimp.

**Building the Blimp**

***1. Connecting the OpenMV Camera with the ESP32 Feather***

The OpenMV Camera and the ESP32 Feather are 2 separate microcontrollers. There are many hardware interfaces that we can use (wired or wireless) to establish a communication protocol between them. For this project, we are using the SPI protocol ([More info on SPI](https://www.electronicshub.org/basics-serial-peripheral-interface-spi/#:~:text=Serial%20Peripheral%20Interface%20or%20SPI,a%20microcontroller%20and%20its%20peripherals.)) with the ESP32 Feather being the Master device and the OpenMV being the Slave device. In order to wire them up, the following needs to be connected from ESP32 Feather to OpenMV:

- MOSI (Pin 18) to P0
- MISO (Pin 19) to P1
- SCK (Pin 5) to P2
- Input (Pin 13) to P3 (Pin 13 as an input can be change in the code in case it is unavailable)
- GND to GND

***2. Setting up the Brushless DC Motors and the ESCs***

By default, the brushless motors are configured in such a way that they rotate in a single direction when providing an input from 1000ms (low) to 2000ms (high). Therefore, in order to use it on our blimp, we first have to reconfigure the ESC so that it runs the BLDC bidirectionally. In order to do that, you would need an Arduino Board, a couple of jumper wires, and a USB cable to connect the board to your laptop. Follow the following steps:
    
- Install the BLHeli Suite Software at: https://github.com/bitdump/BLHeli 
- Flashing the Arduino: First, connect the Arduino to your computer. Then, open BLHeliSuite. Select the Port that your Arduino is connected on in the bottom text input. Then, go to the Make interfaces tab. Select your board from the dropdown and choose the correct baudrate (115200 for our Nano). Click Arduino 4way-interface. You will be asked to select the firmware binary file, which was <BLHeliRootFolder>/Interfaces/Arduino4w-if/. If flashing is successful, AVRDude will say "nnnn bytes of flash verified done. Thank you."
- Wiring the ESC and Arduino: The Arduino will be powered from the USB bus. DO NOT CONNECT THE BATTERY + TERMINAL TO THE ARDUINO. Connect the negative of the battery to the Arduino ground and ESC power ground. Connect the positive of the battery to the ESC power positive. Finally, connect the PWM/digital pin of the ESC to any pin from 11-13 on the Arduino Uno (You can connect and program multiple ESCs at the same time). Optionally, connect a BLDC motor to the ESC, which will emit a noise upon ESC boot-up to tell you that you've connected the power properly.
- Connecting and Programming: Go to the Make sure that the correct serial/COM port is selected at the bottom of the window, and ensure the baudrate is correct i.e. 115200 for our Uno. From the Select ATMEL / SILABS Interface menu, choose D SILABS BLHeli Bootloader (4way-if). Click the Connect button at the bottom of the screen, then click the Check button. The application should tell you which channel you've connected your ESC to. You can click Read Setup to see what the ESC's current settings are. You can configure your settings and then click Write Setup to program the ESC with the settings you've chosen.
- The Configuration: Bidirectional, 1000/1500/2000 milliseconds Min/Center/Max, Startup Beep 100, others Default
- Note: the ESC center would not exactly be at 1500. There is usually a shift in the center value, usually making it less due to the imperfection of the ESC itself. Therefore, if you want symmetry in your autonomous code, it would be a better idea to set the center higher in such a way that it actual center value is 1500. In this case, our center value was configured to 1612.

For more details on how to generally configure ESCs: https://oscarliang.com/flash-blheli-s-esc-firmware-fc-pass-through/

Credits to Kevin Zhu from the GMU Blimp Team for this explanation. Some part of the explanation was altered to fit the setup that the UCLA Team has.

***3. Connecting the Brushless DC Motors with the ESP32 Servo Shield***

By default, these are the pin configurations for connecting the BLDC to the Servo Shield:

- Pin 7 to Left Motor
- Pin 6 to Right Motor
- Pin 5 to Vertical Motor

Connect the white wires from the ESC to the PWM pins on shield. Then, connect the black wires to the GND pins on the shield

***4. Powering the Devices***



***5. Attaching the Electronic Components onto the Blimp Balloon***

**Main Base and PS4 Controller**

The Main Base is an ESP32 Feather that is connected to a laptop so that it can display on the Serial Monitor. It is used to connect directly to the PS4 controller using Bluetooth. Therefore, the first thing that we want to do is to make sure that we can pair the PS4 controller with the feather. 

- Check to see what the MAC address the controller is trying to connect to. This will be the last device that the PS4 was paired to. In order to figure out or change the MAC address, you can use the following tool: https://sixaxispairtool.en.lo4d.com/windows 
- Once you have the MAC address that the controller is trying to connect to, keep that MAC address somewhere as you will need it in the upcoming steps.
- In the [main_base.ino](https://git.uclalemur.com/shahrulkamil98/dtr-spring-2022/-/blob/main/Desktop/main_base/main_base/main_base.ino) code, there will be a line 
- Upload the [main_base.ino](https://git.uclalemur.com/shahrulkamil98/dtr-spring-2022/-/blob/main/Desktop/main_base/main_base/main_base.ino) code into the ESP32 Feather conected to your laptop. 
- In th 

**Behaviour at the Start**

**Possible Error/Issue and Possible Solutions to them**


